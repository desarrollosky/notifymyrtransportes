const index = "/notifyMyRTransportes/";

module.exports = {

    isLoggedIn(req, res, next){
        if(req.isAuthenticated()){
            return next();
        }
        return res.redirect(index + 'login/signin');
    }, 
    isNotLoggedIn(req, res, next){
        if(!req.isAuthenticated()){
            return next();
        }
        return res.redirect(index + 'notificaciones/index');
    }

};