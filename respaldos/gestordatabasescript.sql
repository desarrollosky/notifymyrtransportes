
DROP TABLE IF EXISTS unidad;

CREATE TABLE unidad (
    numero_unidad int NOT NULL PRIMARY KEY,
    nombre varchar(60)
);

DROP TABLE IF EXISTS estado;

CREATE TABLE estado (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre varchar(30)
);

DROP TABLE IF EXISTS actividad;

CREATE TABLE actividad (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre varchar(30)
);

DROP TABLE IF EXISTS comunicacion;

CREATE TABLE comunicacion (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre varchar(30)
);

DROP TABLE IF EXISTS empresa;

CREATE TABLE empresa (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre varchar(30)
);

DROP TABLE IF EXISTS comunicacion_por_empresa;

CREATE TABLE comunicacion_por_empresa (
    idEmpresa int NOT NULL,
    idComunicacion int NOT NULL,
    FOREIGN KEY (idEmpresa) REFERENCES empresa(id),
    FOREIGN KEY (idComunicacion) REFERENCES comunicacion(id)
);

DROP TABLE IF EXISTS actividad_por_empresa;

CREATE TABLE actividad_por_empresa (
    idEmpresa int NOT NULL,
    idActividad int NOT NULL,
    FOREIGN KEY (idEmpresa) REFERENCES empresa(id),
    FOREIGN KEY (idActividad) REFERENCES actividad(id) 
);

DROP TABLE IF EXISTS notificacion;

CREATE TABLE notificacion (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idUnidad int NOT NULL,
    estado int NOT NULL,
    fecha DATETIME,
    nombre varchar(100),
    texto text,
    latitude float,
    longitude float,
    grupo text,
    FOREIGN KEY (idUnidad) REFERENCES unidad(numero_unidad),
    FOREIGN KEY (estado) REFERENCES estado(id)
);

DROP TABLE IF EXISTS comentario;

CREATE TABLE comentario (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idNotificacion int NOT NULL,
    idComunicacion int,
    idActividad int NOT NULL,
    texto text,
    fecha DATETIME,
    FOREIGN KEY (idNotificacion) REFERENCES notificacion(id),
    FOREIGN KEY (idComunicacion) REFERENCES comunicacion(id),
    FOREIGN KEY (idActividad) REFERENCES actividad(id)
);



DROP TABLE IF EXISTS rol;

CREATE TABLE rol (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre varchar(30)
);

DROP TABLE IF EXISTS permiso;

CREATE TABLE permiso (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre varchar(30)
);

DROP TABLE IF EXISTS permiso_por_rol;

CREATE TABLE permiso_por_rol (
    idRol int NOT NULL,
    idPermiso int NOT NULL,
    FOREIGN KEY (idRol) REFERENCES rol(id),
    FOREIGN KEY (idPermiso) REFERENCES permiso (id) 
);

DROP TABLE IF EXISTS usuario;

CREATE TABLE ususario (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idRol int NOT NULL,
    idEmpresa int NOT NULL,
    nombre varchar(30),
    apellido varchar(30),
    username varchar(30),
    password varchar(255),
    estado tinyint(1),
    FOREIGN KEY (idRol) REFERENCES rol (id),
    FOREIGN KEY (idEmpresa) REFERENCES empresa(id)
);
