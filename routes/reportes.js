const { isLoggedIn } = require('../config/auth');
const connection = require('../config/db');

module.exports = function (express, globals) {
    const router = express.Router();
    routes = {
        index: function (req, res, next) {
            content = {
                title: 'Reportes',
                globals: encodeURIComponent(JSON.stringify(globals)),
            };
            res.render('reportes/index', content);
        }
    };
    router.get('/index', isLoggedIn, routes.index);

    return router;
};

module.exports.estadisticas_grupos = function (req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot2 = req.body.id;
    var data = {};

    connection.query("SELECT count(*) as urgente from notificacion WHERE estado=1; SELECT count(*) as proceso from notificacion WHERE estado=2; SELECT count(*) as concluido from notificacion WHERE estado=3;", function (err, rows, fields) {
        if (err) { throw err; } else {

            data = {
                urgente: rows[0],
                proceso: rows[1],
                concluido: rows[2]
            }
            res.json(data);
        }
    });
}

module.exports.graficas_barra = function (req, res) {

    /*
    #(1)Detención En Ruta.~
    #(2)Desvío de Ruta.~
    1. Alerta SOS~
    10. Exceso de velocidad superior a 100 km/hr~
    */

    res.setHeader('Content-type', 'text/plain');
    let inicio = req.body.inicio;
    let final = req.body.final;
    var sql = "";
    var data = {
        "bloqjammer": "",
        "confjammer": "",
        "detruta": "",
        "blocjammerdes": ""
    };

    if ((inicio === undefined || inicio === null || inicio === "") && (final === undefined || final === null || final === "")) {
        //Presiones SOS
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='#[JD] Bloqueo por detección de Jammer fuera de zonas conocidas~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Detención en ruta
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='#Configuración de bloqueo por jammer Activada 1~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Desvío de Ruta
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='Detención en ruta por 10 minutos~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Exceso de velocidad
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='#Configuración de bloqueo por jammer desactivada 1C~' OR nombre='#Configuración de bloqueo por jammer desactivada 1N~' OR nombre='#Configuración de bloqueo por jammer desactivada 1R~' OR nombre='#Configuración de bloqueo por jammer desactivada 1TR~' GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        

        connection.query(sql, function (err, rows, fields) {
            if (err) { throw err; } else {
                data["bloqjammer"] = rows[0];
                data["confjammer"] = rows[1];
                data["detruta"] = rows[2];
                data["blocjammerdes"] = rows[3];
                res.json(data);
            }
        });
    } else {
        inicio = inicio + " 00:00:00";
        final = final + " 23:59:59";

        //Presiones SOS
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='#[JD] Bloqueo por detección de Jammer fuera de zonas conocidas~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Detención en ruta
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='#Configuración de bloqueo por jammer Activada 1~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Desvío de ruta
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='Detención en ruta por 10 minutos~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        //Exceso de velocidad
        sql += "SELECT idUnidad, COUNT(idUnidad) cantidad FROM notificacion WHERE nombre='#Configuración de bloqueo por jammer desactivada 1C~' OR nombre='#Configuración de bloqueo por jammer desactivada 1N~' OR nombre='#Configuración de bloqueo por jammer desactivada 1R~' OR nombre='#Configuración de bloqueo por jammer desactivada 1TR~' AND fecha BETWEEN ? AND ? GROUP BY idUnidad having cantidad >=1 ORDER BY cantidad DESC LIMIT 10;";
        
        connection.query(sql, [inicio, final, inicio, final, inicio, final, inicio, final, inicio, final, inicio, final, inicio, final, inicio, final], function (err, rows, fields) {
            if (err) { throw err; } else {
                data["bloqjammer"] = rows[0]; 
                data["confjammer"] = rows[1];
                data["detruta"] = rows[2]; 
                data["blocjammerdes"] = rows[3];
                res.json(data);
            }
        });
    }
}

module.exports.promedio_cierre = function (req, res) {
    res.setHeader('Content-type', 'text/plain');
    var sql = "";
    var data = {
        "cierre": "",
    };

    sql += "SELECT COUNT(*) totalN, ROUND(((SELECT COUNT(estado) concluidas FROM notificacion WHERE estado='3')/(SELECT COUNT(*) totalnotif from notificacion) * 100),2) AS promedioCierre FROM notificacion;";

    connection.query(sql, function (err, rows, fields) {
        if (err) { throw err; } else {
            data["cierre"] = rows[0];
            res.json(data);
        }
    });
}

module.exports.promedio_asistencia = function (req, res) {
    res.setHeader('Content-type', 'text/plain');
    var sql = "";
    var data = {
        "asistencia": "",
    };

    sql += "SELECT COUNT(*) totalN, ROUND(((SELECT COUNT(DISTINCT(idNotificacion)) asistencia FROM comentario)/(SELECT COUNT(*) totalnotif from notificacion) * 100),2) AS promedioAsist FROM notificacion";

    connection.query(sql, function (err, rows, fields) {
        if (err) { throw err; } else {
            data["asistencia"] = rows[0];
            res.json(data);
        }
    });
}