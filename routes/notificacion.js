const connection = require('../config/db');
var express = require('express');
var app = express();
const { isLoggedIn } = require('../config/auth');
const router = express.Router();
var moment = require('moment');

const index = "/notifyMyRTransportes/";

module.exports = function(express, globals, port) {

    routes = {
        index: function(req, res, next) {
            content = {
                title: 'Notificaciones',
                globals: encodeURIComponent(JSON.stringify(globals)), //Utilizado para transferir las variables globales a la vista y poder utilizarlas en
            };
        },
        notificaciones: function(req, res, next) {
            let sql = "SELECT id, idUnidad, nombre as nombreNotif, texto, latitude, longitude, grupo, fecha, (SELECT nombre FROM estado WHERE estado.id=notificacion.estado) AS nombreEst, (SELECT clase FROM estado WHERE estado.id=notificacion.estado) AS clase, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1), fecha)) as apc, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha DESC LIMIT 1), (SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1))) AS auc, (SELECT CONCAT(nombre,' ',apellido) FROM comentario, usuario WHERE comentario.idNotificacion=notificacion.id AND comentario.idUsuario=usuario.id ORDER BY fecha DESC LIMIT 1) AS usuario FROM notificacion WHERE estado=1 OR estado=2 ORDER BY estado ASC, id DESC;";
            sql += "SELECT * FROM actividad;";
            sql += "SELECT * FROM estado LIMIT 3;";
            sql += "SELECT * FROM comunicacion;";

            let query = connection.query(sql, (err, results) => {

                if (err) {
                    throw err;
                } else {
                    res.render('notificacion/index', {
                        results: results[0],
                        actividades: results[1],
                        estados: results[2],
                        comunicacion: results[3]
                    });
                }

            });

        }
    };

    router.get('/index', isLoggedIn, (routes.index, routes.notificaciones));
    return router;
}

//SUBE LOS COMENTARIOS
module.exports.post_agregarComent = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot = req.body.idNot;
    let actividad = req.body.actividad;
    let estado = req.body.estado;
    let aviso = req.body.aviso;
    let comunicacion = req.body.comunicacion;
    let comentarios = req.body.comentarios;
    let user = req.user.id;

    var datetime = new Date();
    var day = datetime.getDate();
    var month = datetime.getMonth() + 1;
    var year = datetime.getFullYear();
    var hour = datetime.getHours();
    var minutes = datetime.getMinutes();
    var seconds = datetime.getSeconds();

    var fecha = year + '-' + month + '-' + day + ' ' + hour + ':' + minutes + ':' + seconds;

    if (user === "" || user === false) {
        req.flash('error', 'Se encontró un error en el proceso');
        res.redirect(index + 'notificaciones/index');
    } else {
        if (aviso === "si") {
            connection.query("INSERT INTO comentario (idNotificacion, idUsuario, idComunicacion, idActividad, texto, fecha) VALUES (?, ?, ?, ?, ?, ?); UPDATE notificacion SET estado =?, texto=? WHERE id =?", [idNot, user, comunicacion, actividad, comentarios, fecha, estado, comentarios, idNot], function(err, result) {
                if (err) {
                    req.flash('error', 'Se encontró un error en el proceso');
                    res.redirect(index + 'notificaciones/index');
                } else {
                    req.flash('success', 'El comentario se agregó correctamente');
                    res.redirect(index + 'notificaciones/index');
                }
            });
        } else {
            connection.query("INSERT INTO comentario (idNotificacion, idUsuario, idComunicacion, idActividad, texto, fecha) VALUES (?, ?, ?, ?, ?, ?); UPDATE notificacion SET estado =?, texto=? WHERE id =?", [idNot, user, '5', actividad, comentarios, fecha, estado, comentarios, idNot], function(err, result) {
                if (err) {
                    req.flash('error', 'Se encontró un error en el proceso');
                    res.redirect(index + 'notificaciones/index');
                } else {
                    req.flash('success', 'El comentario se agregó correctamente');
                    res.redirect(index + 'notificaciones/index');
                }
            });
        }
    }
}

//MUESTRA EL HISTORIAL
module.exports.post_historialComent = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot2 = req.body.id;
    var data = {
        "coment": "",
        "msj": ""
    };

    connection.query("SELECT comentario.fecha, comunicacion.nombre as comunicacion, actividad.nombre as nombreAct, comentario.texto, usuario.nombre, usuario.apellido FROM comentario, usuario, comunicacion, actividad WHERE comentario.idUsuario=usuario.id AND comunicacion.id=comentario.idComunicacion AND comentario.idActividad=actividad.id AND comentario.idNotificacion=? ORDER BY fecha DESC;", [idNot2], function(err, rows, fields) {
        if (err) { throw err; } else {
            if (rows.length != 0) {
                data["coment"] = rows; 
                res.json(data);
            } else {
                data["msj"] = 'No existen comentarios en la notificación';
                res.json(data);
            }
        }
    });
}

//INSERTA NOTIFICACIONES
module.exports.insertar_notificaciones = async function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let name = req.body.name;
    let lon = req.body.lon;
    let lat = req.body.lat;
    let unidad = req.body.unidad;
    let groups = req.body.groups; 
    let mod = req.body.mod;
    let timeNot = req.body.timeNot;
    var data;

    let fecha = moment.unix(timeNot).format('YYYY-MM-DD HH:mm:ss');

    // for (var i=0; i < groups.length; i++) {
    //     document.write(groups[i] + ",");
    // }

    await connection.query("SELECT * FROM notificacion WHERE fecha=? AND idUnidad=? AND nombre=?;", [fecha, unidad, name], async function(err, rows, fields) {
        if (err) { throw err; } else {
            if (rows.length === 0) {
                await connection.query("INSERT INTO notificacion(idUnidad, estado, fecha, nombre, texto, latitude, longitude, grupo) VALUES(?, ?, ?, ?, ?, ?, ?, ?);", [unidad, '1', fecha, name, mod, lat, lon, groups], function(err, rows, fields) {
                    if (err) { throw err; }
                    else {
                        data = 'OK';
                        res.json(data);
                    }
                });
            }
        }
    });
}

module.exports.traer_notificaciones = function(req, res) {
    res.setHeader('Content-type', 'text/plain');

    var data = {
        "results": ""
    };

    let sql = "SELECT id, idUnidad, nombre as nombreNotif, texto, latitude, longitude, grupo, fecha, (SELECT nombre FROM estado WHERE estado.id=notificacion.estado) AS nombreEst, (SELECT clase FROM estado WHERE estado.id=notificacion.estado) AS clase, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1), fecha)) as apc, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha DESC LIMIT 1), (SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1))) AS auc, (SELECT CONCAT(nombre,' ',apellido) FROM comentario, usuario WHERE comentario.idNotificacion=notificacion.id AND comentario.idUsuario=usuario.id ORDER BY fecha DESC LIMIT 1) AS usuario FROM notificacion WHERE estado=1 ORDER BY estado ASC, id DESC;";

    connection.query(sql, function(err, rows, fields) {
        if (err) { throw err; } else {
            if (rows.length != 0) {
                data["results"] = rows;
                res.json(data);
            }
        }
    });
}

module.exports.enviarNoti_papelera = function(req, res) {
    res.setHeader('Content-type', 'text/plain');

    connection.query("UPDATE notificacion SET estado=4 where estado=1;", function(err, result) {
        if (err) {
            console.log(err);
            /*data = 'Error';
            res.json(data);*/
             req.flash('error', 'Se encontró un error en el proceso');
            res.redirect(index + 'notificaciones/index');
        } else {
            /*data = 'OK';
            res.json(data);*/
            req.flash('success', 'Las notificaciones fueron enviados a papelera correctamente');
            res.redirect(index + 'notificaciones/index');
        }
    });

}