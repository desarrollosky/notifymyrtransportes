const passport = require('passport');
const { isNotLoggedIn } = require('../config/auth');
const nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');
const connection = require('../config/db');

const index = "/notifyMyRTransportes/";

module.exports = function (express) {
    const router = express.Router();

    router.get('/signin', isNotLoggedIn, (req, res) => {
        res.render('login/index');
    });

    return router;
}

module.exports.sessionStart = function (req, res) {
    passport.authenticate('local.signin', {
        successRedirect: '/notifyMyRTransportes/notificaciones/index',
        failureRedirect: '/notifyMyRTransportes/login/signin',
        failureFlash: true
    })(req, res)
}

module.exports.sendMail = function (req, res) {
    res.setHeader('Content-type', 'text/plain');

    let mail = req.body.email;

    connection.query("SELECT * FROM usuario WHERE username=?;", [mail], function (err, rows) {
        if (err) { throw err; }
        else {
            if (rows.length > 0) {
                const user = rows[0];

                var length = 10;
                var chars = 'Aa#!';
                var mask = '';
                if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
                if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                if (chars.indexOf('#') > -1) mask += '0123456789';
                if (chars.indexOf('!') > -1) mask += '$@$!%*?&';
                var randompass = '';
                for (var i = length; i > 0; --i) randompass += mask[Math.round(Math.random() * (mask.length - 1))];

                //Encrypt
                const saltRounds = 10; //long encrypt
                bcrypt.genSalt(saltRounds, function (err, salt) {
                    if (err) {
                        throw err
                    } else {
                        bcrypt.hash(randompass, salt, function (err, hash) {
                            if (err) {
                                throw err
                            } else {
                                connection.query("UPDATE usuario SET password=? WHERE username=?;", [hash, mail], function (err, rows, fields) {
                                    if (err) {
                                        req.flash('error', 'Se encontró un error en el proceso');
                                        res.redirect(index + 'login/signin');
                                    } else {
                                        //Send Email
                                        const contentMail = `
                                            <div style="font-family: "Times New Roman", Times, serif;">
                                                <h2 style="padding-bottom: 28px;">Recuperación de contraseña</h2>

                                                <p style="padding-bottom: 18px;">Estimado(a) usuario</p>

                                                <p>Se ha modificado la contraseña de acceso en el Gestor de Notificaciones. Para acceder a la cuenta ingrese con la nueva contraseña: <b>${randompass}</b></p>
                                                
                                                <p style="padding-bottom: 18px;">Si desea modificar la contraseña ingrese a "Mi cuenta" dentro del menú principal e ir a la sección "Actualizar contraseña".</p>

                                                <p>Aviso: Por favor no responder a este mensaje, el correo es solo informativo. Muchas gracias por su atención.</p>
                                            </div>
                                        `;

                                        const transporter = nodemailer.createTransport({
                                            host: 'mail.skyguardian.mx',
                                            port: 465,
                                            secure: true, // true for 465, false for other ports
                                            auth: {
                                                user: 'developer3@skyguardian.mx',
                                                pass: 'OH#%85f??2Q&'
                                            }
                                        });

                                const info = transporter.sendMail({
                                    from: "'Desarrollo Skyguardian' <developer3@skyguardian.mx>",
                                    to: mail,
                                    subject: 'Su contraseña a sido modificada',
                                    html: contentMail
                                });

                                req.flash('success', 'Se envió correo con su nueva contraseña, favor de verificar en la bandeja de entrada.');
                                res.redirect(index + 'login/signin');
                            }
                        });
                    }
                });
            }
        });
} else {
    req.flash('error', 'El correo no se encuentra asignado a ninguna cuenta.');
    res.redirect(index + 'login/signin');
}
        }
    });
}